----------------------------------------------------------------------------------
--                                 _             _
--                                | |_  ___ _ __(_)__ _
--                                | ' \/ -_) '_ \ / _` |
--                                |_||_\___| .__/_\__,_|
--                                         |_|
--
----------------------------------------------------------------------------------
--
-- Company: hepia
-- Author: Joachim Schmidt <joachim.schmidt@hesge.ch>
--
-- Module Name: image_generator - behavioural
-- Target Device: All
-- Tool version: 2018.3
-- Description: Image Generator
--
-- Last update: 2019-02-15
--
---------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use ieee.numeric_std.all;
use ieee.std_logic_signed.all;

library work;
use work.hdmi_interface_pkg.all;

entity image_generator is

		generic (
						C_DATA_SIZE  : integer     := 16;
						C_PIXEL_SIZE : integer     := 8;
						C_VGA_CONFIG : t_VgaConfig := C_DEFAULT_VGACONFIG);

		port (
					 ClkVgaxCI    : in  std_logic;
					 RstxRAI      : in  std_logic;
					 PllLockedxSI : in  std_logic;
					 HCountxDI    : out  std_logic_vector((C_DATA_SIZE - 1) downto 0);
					 VCountxDI    : out  std_logic_vector((C_DATA_SIZE - 1) downto 0);
					 VidOnxSI     : in  std_logic;
					 DataxDO      : out std_logic_vector(((C_PIXEL_SIZE * 3) - 1) downto 0);

					 we           : out std_logic;
					 juliaset : in  std_logic;
					 D_i         : in    std_logic;
					 L_i         : in    std_logic;
					 R_i         : in    std_logic;
					 U_i         : in    std_logic;

					 Color1xDI    : in  std_logic_vector(((C_PIXEL_SIZE * 3) - 1) downto 0));

end entity image_generator;

architecture behavioural of image_generator is

		component mandelbrot_calcul is
				generic (
								comma : integer := 12;
								maxiter : integer := 100;
								SIZE : integer := 16
						); -- 0000, 0000 0000 0000
						   -- 0100,000000000100
				Port (
							 clk : in STD_LOGIC;
							 rst : in STD_LOGIC;
							 ready : out STD_LOGIC;
							 start : in STD_LOGIC;
							 finished : out STD_LOGIC;
							 c_real : in STD_LOGIC_VECTOR (SIZE-1 downto 0);
							 c_imaginary : in STD_LOGIC_VECTOR (SIZE-1 downto 0);
							 z_real : out STD_LOGIC_VECTOR (SIZE-1 downto 0);
							 z_imaginary : out STD_LOGIC_VECTOR (SIZE-1 downto 0);
							 iteration : out STD_LOGIC_VECTOR (SIZE-1 downto 0)
					 );
		end component mandelbrot_calcul;

		component ComplexValueGenerator is
				generic(
							   SIZE        : integer :=  16;  -- Taille en bits de nombre au format virgule fixe
							   COMMA       : integer :=  12;  -- Nombre de bits après la virgule
							   X_SIZE      : integer := 300;  -- Taille en X (Nombre de pixel) de la fractale à afficher
							   Y_SIZE      : integer := 200;  -- Taille en Y (Nombre de pixel) de la fractale à afficher
							   SCREEN_RES  : integer := 10    -- Nombre de bit pour les vecteurs X et Y de la position du pixel
					   );   
				port(
							clk         : in  std_logic;
							reset       : in  std_logic;
							-- interface avec le module MandelbrotMiddleware
							next_value  : in  std_logic;
							c_real      : out std_logic_vector (SIZE-1 downto 0);
							c_imaginary : out std_logic_vector (SIZE-1 downto 0);
							X_screen    : out std_logic_vector (SCREEN_RES-1 downto 0);
							Y_screen    : out std_logic_vector (SCREEN_RES-1 downto 0)
					);
		end component ComplexValueGenerator;
		component color_lut is

				generic (
								C_DATA_SIZE  : integer     := 16;
								C_PIXEL_SIZE : integer     := 8;
								C_VGA_CONFIG : t_VgaConfig := C_DEFAULT_VGACONFIG);

				port (
							 iteration_i    : in  std_logic_vector((C_DATA_SIZE - 1) downto 0);
							 DataxD       : out std_logic_vector(((C_PIXEL_SIZE * 3) - 1) downto 0));

		end component color_lut;


		signal ready_s : STD_LOGIC;
		signal start_s : STD_LOGIC;
		signal finished_s : STD_LOGIC;

		signal transfered_s : STD_LOGIC;
		signal next_value_s : STD_LOGIC;

		signal db_s : STD_LOGIC;

		signal x_screen_s : std_logic_vector(16-1 downto 0);
		signal y_screen_s : std_logic_vector(16-1 downto 0);


		--		signal x_cc : signed(16 downto 0);
		--		signal y_cc : signed(16 downto 0);
		signal increment : std_logic_vector(C_DATA_SIZE-1 downto 0) := "0000000100000000";

		signal x_iter : std_logic_vector(C_DATA_SIZE-1 downto 0);
		signal y_iter : std_logic_vector(C_DATA_SIZE-1 downto 0);

		signal cr_s : std_logic_vector(C_DATA_SIZE-1 downto 0);
		signal ci_s : std_logic_vector(C_DATA_SIZE-1 downto 0);

		signal zr_s : std_logic_vector(C_DATA_SIZE-1 downto 0);
		signal zi_s : std_logic_vector(C_DATA_SIZE-1 downto 0);

		signal iteration_s : STD_LOGIC_VECTOR(C_DATA_SIZE-1 downto 0);

		signal x_c : STD_LOGIC_VECTOR(C_DATA_SIZE-1 downto 0);
		signal y_c : STD_LOGIC_VECTOR(C_DATA_SIZE-1 downto 0);

		signal VgaConfigxD            : t_VgaConfig                                         := C_VGA_CONFIG;
		signal DataxD                 : std_logic_vector(((C_PIXEL_SIZE * 3) - 1) downto 0) := (others => '0');
		signal HCountxD               : std_logic_vector((C_DATA_SIZE - 1) downto 0)        := (others => '0');
		signal VCountxD               : std_logic_vector((C_DATA_SIZE - 1) downto 0)        := (others => '0');

begin  -- architecture behavioural

		-- Asynchronous statements

		assert (C_VGA_CONFIG = C_640x480_VGACONFIG)
		or (C_VGA_CONFIG = C_800x600_VGACONFIG)
		or (C_VGA_CONFIG = C_1024x600_VGACONFIG)
		or (C_VGA_CONFIG = C_1024x768_VGACONFIG)
		report "Not supported resolution!" severity failure;

		ImGenSigOutxB : block is
		begin  -- block ImGenSigOutxB

		--DataxAS   : DataxDO  <= DataxD;
		HCountxAS : HCountxD <= HCountxDI;
		VCountxAS : VCountxD <= VCountxDI;

end block ImGenSigOutxB;


calculateur : entity work.mandelbrot_calcul
generic map (
					comma => 12,
					maxiter => 100,
					SIZE => 16) -- 0000, 0000 0000 0000
								-- 0100,000000000100
Port map (
				 clk         => ClkVgaxCI,
				 rst         => RstxRAI,
				 ready       => ready_s,
				 start       => '1',
				 finished    => finished_s,
				 c_real      => cr_s,
				 c_imaginary => ci_s,
				 z_real      => zr_s,
				 z_imaginary => zi_s,
				 iteration   => iteration_s
		 );

cr_s <= x_c when juliaset = '0' else x_iter;
ci_s <= y_c when juliaset = '0' else y_iter;
zr_s <= x_iter when juliaset = '0' else x_c;
zi_s <= y_iter when juliaset = '0' else y_c;

Generateur : entity work.ComplexValueGenerator
generic map(
				   SIZE       => 16, 
				   COMMA      => 12,
				   X_SIZE     => 1024,
				   Y_SIZE     => 600,
				   SCREEN_RES => 16
		   )  
port map(
				clk         =>   ClkVgaxCI,
				reset       =>   RstxRAI,
				next_value  =>   finished_s,
				c_real      =>   x_c,
				c_imaginary =>   y_c,
				X_screen    =>   x_screen_s,
				Y_screen    =>   y_screen_s
		);
color_lookuptable : entity work.color_lut
port map (
				 iteration_i => iteration_s,
				 DataxD      => DataxDO
		 );


db_s <= L_i or R_i or U_i or D_i;

process(db_s,L_i,R_i,U_i,D_i,juliaset)
begin
		if juliaset = '0' then
				x_iter <= (others => '0');
				y_iter <= (others => '0');
		elsif rising_edge(db_s) then
				if L_i = '1' then
						x_iter <= std_logic_vector(signed(x_iter) - signed(increment));
				elsif R_i = '1' then
						x_iter <= std_logic_vector(signed(x_iter) + signed(increment));
				elsif D_i = '1' then
						y_iter <= std_logic_vector(signed(y_iter) - signed(increment));
				elsif U_i = '1' then
						y_iter <= std_logic_vector(signed(y_iter) + signed(increment));
				end if;

		end if;

end process;

HCountxDI <= x_screen_s;
VCountxDI <= y_screen_s;
we <= finished_s;

end architecture behavioural;
