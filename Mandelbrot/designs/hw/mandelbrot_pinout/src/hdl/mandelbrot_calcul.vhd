----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.03.2019 18:33:31
-- Design Name: 
-- Module Name: mandelbrot_calcul - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity mandelbrot_calcul is
		generic (
						comma : integer := 12;
						maxiter : integer := 100;
						SIZE : integer := 16); -- 0000, 0000 0000 0000
											   -- 0100,000000000100
		Port (
					 clk : in STD_LOGIC;
					 rst : in STD_LOGIC;
					 ready : out STD_LOGIC;
					 start : in STD_LOGIC;
					 finished : out STD_LOGIC;
					 c_real : in STD_LOGIC_VECTOR (SIZE-1 downto 0);
					 c_imaginary : in STD_LOGIC_VECTOR (SIZE-1 downto 0);
					 z_real : in STD_LOGIC_VECTOR (SIZE-1 downto 0);
					 z_imaginary : in STD_LOGIC_VECTOR (SIZE-1 downto 0);
					 iteration : out STD_LOGIC_VECTOR (SIZE-1 downto 0)
			 );
end mandelbrot_calcul;

architecture Behavioral of mandelbrot_calcul is

  -- Size constants
		constant SIZE_BIG           : integer := 2*SIZE;
		constant SIZE_IN_BIG        : integer := comma+SIZE;
		constant COMMA_BIG          : integer := 2*comma;
		constant SIZE_RADIUS        : integer := 2*(SIZE-comma);
		constant EXTEND_COMMA       : std_logic_vector(comma-1 downto 0) := (others => '0');

		signal one_is_finished_s    : std_logic;


  -- Calculation signals
		signal z_real_s             : std_logic_vector(SIZE-1 downto 0);
		signal zn1_real_big_s       : std_logic_vector(SIZE_BIG-1 downto 0);
		signal z_imag_s             : std_logic_vector(SIZE-1 downto 0); 
		signal zn1_imag_big_s       : std_logic_vector(SIZE_BIG-1 downto 0);
		signal z_real2_big_s        : std_logic_vector(SIZE_BIG-1 downto 0);
		signal zn1_real2_big_s      : std_logic_vector(SIZE_BIG-1 downto 0);
		signal z_r2_i2_big_s        : std_logic_vector(SIZE_BIG-1 downto 0);
		signal zn1_r2_i2_big_s      : std_logic_vector(SIZE_BIG-1 downto 0); 
		signal z_ri_big_s           : std_logic_vector(SIZE_BIG-1 downto 0);
		signal zn1_ri_big_s         : std_logic_vector(SIZE_BIG-1 downto 0);
		signal z_imag2_big_s        : std_logic_vector(SIZE_BIG-1 downto 0);
		signal zn1_imag2_big_s      : std_logic_vector(SIZE_BIG-1 downto 0);
		signal z_2ri_big_s          : std_logic_vector(SIZE_BIG-1 downto 0);
		signal zn1_2ri_big_s        : std_logic_vector(SIZE_BIG-1 downto 0);

		signal n1_radius_big_s      : std_logic_vector(SIZE_BIG downto 0);    
		signal radius_s             : std_logic_vector(SIZE_RADIUS downto 0);

		type stateMoore_type is (ready_m, compute_m, done_m); -- 3 states are required for Moore
		signal stateMoore_reg, stateMoore_next : stateMoore_type;

		signal compute_real : signed(c_real'high downto c_real'low);
		signal compute_img : signed(c_imaginary'high downto c_imaginary'low);
		signal iteration_s : signed(c_imaginary'high downto c_imaginary'low);
		signal argc2_s : unsigned(c_imaginary'high downto c_imaginary'low);
		signal argc2_temp_s : unsigned(c_imaginary'high downto c_imaginary'low);

		signal c_real_s : signed(c_real'high downto c_real'low);
		signal c_img_s : signed(c_imaginary'high downto c_imaginary'low);

		signal radius_stop_s : signed(SIZE-1 downto comma);
		signal radius_stop_temp_s	: signed(SIZE-1 downto comma);

		signal compute_real_temp : signed(c_real'high*2+1 downto c_real'low);
		signal compute_img_temp  : signed(c_real'high*2+1 downto c_real'low);

		signal ready_s : std_logic;
		signal finished_s : std_logic;
begin

		process(rst,clk)
		begin
				if rst = '1' then
						ready <= '1';
			--finished <= '0';
			--iteration <= (others => '0');
						compute_real <= (others => '0'); -- TODO : Julia set : init with non-zero value
						compute_img <= (others => '0'); -- TODO : Julia set : init with non-zero value
						stateMoore_reg <= ready_m;
				elsif rising_edge(clk) then
						ready <= ready_s;
						stateMoore_reg <= stateMoore_next;
						compute_real <= compute_real_temp(compute_real'high downto compute_real'low);
						compute_img <= compute_img_temp(compute_img'high downto compute_img'low);
				end if;
		end process;

	-- Moore Design
	--process(stateMoore_reg, compute_real, compute_img, iteration_s, start)
		process(clk)
		begin 
				if rising_edge(clk) then
		-- store current state as next
						stateMoore_next <= stateMoore_reg; -- required: when no case statement is satisfied
						finished_s <= '0';

						case stateMoore_reg is 
								when ready_m =>
										stateMoore_next <= compute_m;
										ready_s <= '1';
										iteration_s <= to_signed(0,iteration_s'length);
										z_real_s          <= z_real;
										z_imag_s          <= z_imaginary;
										--z_real_s          <= (others => '0');
										--z_imag_s          <= (others => '0');
										z_real2_big_s  <= (others => '0');   
										z_imag2_big_s  <= (others => '0');   
										z_r2_i2_big_s  <= (others => '0');   
										z_ri_big_s     <= (others => '0');   
										z_2ri_big_s    <= (others => '0');   
										radius_s       <= (others => '0');   
								when compute_m => 
										if(one_is_finished_s = '1') then
												stateMoore_next <= done_m;
										else
												iteration_s <= iteration_s + 1;
												z_real2_big_s     <= zn1_real2_big_s;
												z_imag2_big_s     <= zn1_imag2_big_s;
												z_r2_i2_big_s     <= zn1_r2_i2_big_s;
												z_ri_big_s        <= zn1_ri_big_s;
												z_2ri_big_s       <= zn1_2ri_big_s;
												z_real_s          <= zn1_real_big_s(SIZE_IN_BIG-1 downto comma);
												z_imag_s          <= zn1_imag_big_s(SIZE_IN_BIG-1 downto comma);
												radius_s          <= std_logic_vector(n1_radius_big_s(SIZE_BIG downto COMMA_BIG));
										end if;
										ready_s <= '0';
								when done_m =>
										if(iteration_s >= maxiter) then
												iteration_s <= (others => '0');
										end if;

										ready_s <= '1';
										finished_s <= '1';
										stateMoore_next <= ready_m;

						end case; 
				end if;
		end process;

		iteration <= std_logic_vector(iteration_s);
		finished <= finished_s;


		calc_proc : process (
				iteration_s
		)
		begin
				one_is_finished_s <= '0';

				zn1_real2_big_s   <= std_logic_vector(signed(z_real_s)*signed(z_real_s));
				zn1_imag2_big_s   <= std_logic_vector(signed(z_imag_s)*signed(z_imag_s));
				zn1_r2_i2_big_s   <= std_logic_vector(signed(z_real2_big_s)-signed(z_imag2_big_s));
				zn1_real_big_s  <= std_logic_vector(signed(std_logic_vector'(c_real & EXTEND_COMMA)) + signed(z_r2_i2_big_s));

				zn1_ri_big_s    <= std_logic_vector(signed(z_real_s)*signed(z_imag_s));
				zn1_2ri_big_s   <= z_ri_big_s(SIZE_BIG-2 downto 0) & '0';
				zn1_imag_big_s  <= std_logic_vector(signed(std_logic_vector'(c_imaginary & EXTEND_COMMA)) + signed(z_2ri_big_s));


				n1_radius_big_s    <= std_logic_vector(signed(z_real2_big_s(SIZE_BIG-1) & z_real2_big_s)+signed(z_imag2_big_s(SIZE_BIG-1) & z_imag2_big_s));

				if signed(radius_s) >= 4 or unsigned(iteration_s) >= maxiter then
						one_is_finished_s <= '1';       
				end if;
		end process; 

end Behavioral;
