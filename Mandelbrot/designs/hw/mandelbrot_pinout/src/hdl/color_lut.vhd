
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library work;
use work.hdmi_interface_pkg.all;

entity color_lut is

		generic (
						C_DATA_SIZE  : integer     := 16;
						C_PIXEL_SIZE : integer     := 8;
						C_VGA_CONFIG : t_VgaConfig := C_DEFAULT_VGACONFIG);

		port (
					 iteration_i    : in  std_logic_vector((C_DATA_SIZE - 1) downto 0);
					 DataxD       : out std_logic_vector(((C_PIXEL_SIZE * 3) - 1) downto 0));

end entity color_lut;

architecture behavioural of color_lut is
begin

		DataxD <= x"000000" when iteration_i = x"0000" else
				  x"510000" when iteration_i = x"0001" else
				  x"560000" when iteration_i = x"0002" else
				  x"5b0000" when iteration_i = x"0003" else
				  x"600000" when iteration_i = x"0004" else
				  x"650000" when iteration_i = x"0005" else
				  x"690000" when iteration_i = x"0006" else
				  x"6e0000" when iteration_i = x"0007" else
				  x"730000" when iteration_i = x"0008" else
				  x"780000" when iteration_i = x"0009" else
				  x"7d0000" when iteration_i = x"000a" else
				  x"820000" when iteration_i = x"000b" else
				  x"870000" when iteration_i = x"000c" else
				  x"8c0000" when iteration_i = x"000d" else
				  x"910000" when iteration_i = x"000e" else
				  x"960000" when iteration_i = x"000f" else
				  x"9b0000" when iteration_i = x"0010" else
				  x"a00000" when iteration_i = x"0011" else
				  x"a40000" when iteration_i = x"0012" else
				  x"a90000" when iteration_i = x"0013" else
				  x"ae0000" when iteration_i = x"0014" else
				  x"b30000" when iteration_i = x"0015" else
				  x"b80000" when iteration_i = x"0016" else
				  x"bd0000" when iteration_i = x"0017" else
				  x"c20000" when iteration_i = x"0018" else
				  x"c70000" when iteration_i = x"0019" else
				  x"cc0000" when iteration_i = x"001a" else
				  x"d10000" when iteration_i = x"001b" else
				  x"d60000" when iteration_i = x"001c" else
				  x"db0000" when iteration_i = x"001d" else
				  x"df0000" when iteration_i = x"001e" else
				  x"e40000" when iteration_i = x"001f" else
				  x"e90000" when iteration_i = x"0020" else
				  x"ee0000" when iteration_i = x"0021" else
				  x"f30000" when iteration_i = x"0022" else
				  x"f80000" when iteration_i = x"0023" else
				  x"fd0000" when iteration_i = x"0024" else
				  x"ff0700" when iteration_i = x"0025" else
				  x"ff1300" when iteration_i = x"0026" else
				  x"ff1e00" when iteration_i = x"0027" else
				  x"ff2900" when iteration_i = x"0028" else
				  x"ff3500" when iteration_i = x"0029" else
				  x"ff4000" when iteration_i = x"002a" else
				  x"ff4b00" when iteration_i = x"002b" else
				  x"ff5700" when iteration_i = x"002c" else
				  x"ff6200" when iteration_i = x"002d" else
				  x"ff6d00" when iteration_i = x"002e" else
				  x"ff7900" when iteration_i = x"002f" else
				  x"ff8400" when iteration_i = x"0030" else
				  x"ff8f00" when iteration_i = x"0031" else
				  x"ff9b00" when iteration_i = x"0032" else
				  x"ffa600" when iteration_i = x"0033" else
				  x"ffb100" when iteration_i = x"0034" else
				  x"ffbd00" when iteration_i = x"0035" else
				  x"ffc800" when iteration_i = x"0036" else
				  x"ffd300" when iteration_i = x"0037" else
				  x"ffdf00" when iteration_i = x"0038" else
				  x"ffea00" when iteration_i = x"0039" else
				  x"fff500" when iteration_i = x"003a" else
				  x"fdff00" when iteration_i = x"003b" else
				  x"f2ff00" when iteration_i = x"003c" else
				  x"e7ff00" when iteration_i = x"003d" else
				  x"ddff00" when iteration_i = x"003e" else
				  x"d2ff00" when iteration_i = x"003f" else
				  x"c8ff00" when iteration_i = x"0040" else
				  x"bdff00" when iteration_i = x"0041" else
				  x"b3ff00" when iteration_i = x"0042" else
				  x"a8ff00" when iteration_i = x"0043" else
				  x"9eff00" when iteration_i = x"0044" else
				  x"93ff00" when iteration_i = x"0045" else
				  x"89ff00" when iteration_i = x"0046" else
				  x"7eff00" when iteration_i = x"0047" else
				  x"74ff00" when iteration_i = x"0048" else
				  x"69ff00" when iteration_i = x"0049" else
				  x"5fff00" when iteration_i = x"004a" else
				  x"54ff00" when iteration_i = x"004b" else
				  x"49ff00" when iteration_i = x"004c" else
				  x"3fff00" when iteration_i = x"004d" else
				  x"34ff00" when iteration_i = x"004e" else
				  x"2aff00" when iteration_i = x"004f" else
				  x"1fff00" when iteration_i = x"0050" else
				  x"15ff00" when iteration_i = x"0051" else
				  x"0aff00" when iteration_i = x"0052" else
				  x"00ff00" when iteration_i = x"0053" else
				  x"00ff23" when iteration_i = x"0054" else
				  x"00ff48" when iteration_i = x"0055" else
				  x"00ff6d" when iteration_i = x"0056" else
				  x"00ff92" when iteration_i = x"0057" else
				  x"00ffb7" when iteration_i = x"0058" else
				  x"00ffdc" when iteration_i = x"0059" else
				  x"00feff" when iteration_i = x"005a" else
				  x"00efff" when iteration_i = x"005b" else
				  x"00e0ff" when iteration_i = x"005c" else
				  x"00d1ff" when iteration_i = x"005d" else
				  x"00c3ff" when iteration_i = x"005e" else
				  x"00b4ff" when iteration_i = x"005f" else
				  x"00a5ff" when iteration_i = x"0060" else
				  x"0097ff" when iteration_i = x"0061" else
				  x"0088ff" when iteration_i = x"0062" else
				  x"0079ff" when iteration_i = x"0063" else
				  x"006aff" when iteration_i = x"0064" else
				  x"005cff" when iteration_i = x"0065" else
				  x"004dff" when iteration_i = x"0066" else
				  x"003eff" when iteration_i = x"0067" else
				  x"002fff" when iteration_i = x"0068" else
				  x"0021ff" when iteration_i = x"0069" else
				  x"0012ff" when iteration_i = x"006a" else
				  x"0003ff" when iteration_i = x"006b" else
				  x"0900f8" when iteration_i = x"006c" else
				  x"1400ef" when iteration_i = x"006d" else
				  x"1e00e7" when iteration_i = x"006e" else
				  x"2800de" when iteration_i = x"006f" else
				  x"3100d6" when iteration_i = x"0070" else
				  x"3800cd" when iteration_i = x"0071" else
				  x"4000c4" when iteration_i = x"0072" else
				  x"4600bc" when iteration_i = x"0073" else
				  x"4b00b3" when iteration_i = x"0074" else
				  x"5000ab" when iteration_i = x"0075" else
				  x"5400a2" when iteration_i = x"0076" else
				  x"570099" when iteration_i = x"0077" else
				  x"590091" when iteration_i = x"0078" else
				  x"5a0088" when iteration_i = x"0079" else
				  x"5b0080" when iteration_i = x"007a" else
				  x"5a0077" when iteration_i = x"007b" else
				  x"59006e" when iteration_i = x"007c" else
				  x"570066" when iteration_i = x"007d" else
				  x"54005d" when iteration_i = x"007e" else
				  x"000000";

end architecture behavioural;
